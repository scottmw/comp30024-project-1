"""
COMP30024 Artificial Intelligence, Semester 1, 2021
Project Part A: Searching

This script contains the entry point to the program (the code in
`__main__.py` calls `main()`). Your solution starts here!
"""
import sys
import json

from search.search_algorithm import *


def main():
    # define global variable
    global upperPiecesDict, lowerPiecesDict, targetsDict, setBlocks
    try:
        with open(sys.argv[1]) as file:
            data = json.load(file)
    except IndexError:
        print("usage: python3 -m search path/to/input.json", file=sys.stderr)
        sys.exit(1)

    parse_input(data)
    # Add starting targets
    for piece in upperPiecesDict:
        find_target(piece)
    # keep moving until all the piece met its target
    while targetsDict:
        take_turn()
    file.close()


def parse_input(data):
    """
     data is a dictionary with keys "upper", "lower" and "block"
     pieces in dictionary, blocks in set, we can use if position in setblocks

    parse data into either dictPieces or setBlocks
    :param data: Dictionary obtain after read from JSON file using JSON module.
    :return: NULL
    """
    # We can put the code to read the file NOT in the try/except statement because
    # if the file couldn't be read the process would end anyway

    global upperPiecesDict, lowerPiecesDict, setBlocks, positionHistory
    initialPiecesUpper = data["upper"]
    initialPiecesLower = data["lower"]
    initialBlocks = data["block"]

    keyWrite = ""

    # Parse the Upper's player token
    nump, numr, nums = 0, 0, 0
    for piece in initialPiecesUpper:
        if piece[TYPE] == "p":
            nump = nump + 1
            keyWrite = "P" + str(nump)
        elif piece[TYPE] == "r":
            numr = numr + 1
            keyWrite = "R" + str(numr)
        else:
            nums = nums + 1
            keyWrite = "S" + str(nums)
        upperPiecesDict[keyWrite] = (piece[F_ROW], piece[F_COLUMN])
        positionHistory[keyWrite] = {}

    # parse the Lower player's token
    nump, numr, nums = 0, 0, 0
    for piece in initialPiecesLower:
        if piece[TYPE] == "p":
            nump = nump + 1
            keyWrite = "p" + str(nump)
        elif piece[TYPE] == "r":
            numr = numr + 1
            keyWrite = "r" + str(numr)
        else:
            nums = nums + 1
            keyWrite = "s" + str(nums)
        lowerPiecesDict[keyWrite] = (piece[F_ROW], piece[F_COLUMN])

    # parse the block object
    for block in initialBlocks:
        setBlocks.add((block[F_ROW], block[F_COLUMN]))

    # Situation 1: If you plan the route ahead without knowing any other piece route,
    # there will be a crash at tile N as both R and S try to take a straight line
    #              .-'-._.-'-._.-'-._.-'-._.-'-.
    #             |     |     |     |     |     |
    #           .-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #          |     |  p  |     |     |  s  |     |
    #        .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #       |     |     |     |     |     |     |     |
    #     .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #    |     |     |     |     |     |     |     |     |
    #  .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    # |     |     |     |     |  N  |     |     |     |     |
    # '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #    |     |     |     |     |     |     |     |     |
    #    '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #       |     |     |     |     |     |     |     |
    #       '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #          |     |  R  |     |     |  S |     |
    #          '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #             |      |     |     |     |     |
    #             '-._.-'-._.-'-._.-'-._.-'-._.-'

    # Situation 2 (B are blocks)
    # I'm not sure if they will throw something this tricky at us
    # but this should be solvable
    #              .-'-._.-'-._.-'-._.-'-._.-'-.
    #             |     |     |     |     |     |
    #           .-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #          |     |     |     |     |     |     |
    #        .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #       |     |     |     |     |     |     |     |
    #     .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #    |  B  |  B  |  B  |  B  |  B  |  B  |  B  |     |
    #  .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    # |  B  |  P  |     |     |  S  |  r  |  p  |  B  |     |
    # '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #    |  B  |  B  |  B  |  B  |  B  |  B  |  B  |     |
    #    '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #       |     |     |     |     |     |     |     |
    #       '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #          |     |     |     |     |     |     |
    #          '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #             |     |     |     |     |     |
    #             '-._.-'-._.-'-._.-'-._.-'-._.-'
    # Situation 2.B (B are blocks)
    # how to tell if there are no way to reach the target
    #
    #              .-'-._.-'-._.-'-._.-'-._.-'-.
    #             |     |     |     |     |     |
    #           .-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #          |     |     |     |     |     |     |
    #        .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #       |     |     |     |     |  r  |     |     |
    #     .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #    |  B  |  B  |  B  |  B  |  B  |  B  |  B  |     |
    #  .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    # |  B  |  P  |     |     |  S  |  r  |  p  |  B  |     |
    # '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #    |  B  |  B  |  B  |  B  |  B  |  B  |  B  |     |
    #    '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #       |     |     |     |     |     |     |     |
    #       '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #          |     |     |     |     |     |     |
    #          '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #             |     |     |     |     |     |
    #             '-._.-'-._.-'-._.-'-._.-'-._.-'
    # Scenario 3:
    # can cause a infinite loops.
    #              .-'-._.-'-._.-'-._.-'-._.-'-.
    #             |     |     |     |     |     |
    #           .-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #          |     |     |     |     |     |     |
    #        .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #       |     |     |     |     |     |     |     |
    #     .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    #    |     |     |  R1 |     |     |     |     |     |
    #  .-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-.
    # |     |     |  B  |     |  B  |     |     |     |     |
    # '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #    |     |     |  B  |  B  |  B  |     |     |     |
    #    '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #       |     |     |     |     |     |     |     |
    #       '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #          |     |     |  s1 |     |     |     |
    #          '-._.-'-._.-'-._.-'-._.-'-._.-'-._.-'
    #             |     |     |     |     |     |
    #             '-._.-'-._.-'-._.-'-._.-'-._.-'
