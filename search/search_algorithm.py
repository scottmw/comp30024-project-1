"""
This module is hold all method relate to Search algorithm
"""
from typing import Dict, Set, List

from search.movement_logic import *
from search.search_algo import piece_collision
import main
# Constant definition:
from search.util import *

# Global variable turn is needed to print output
turn = 0

BLOCK = "[X]"
UPPER_ROCK = 'R'
UPPER_SCISSOR = 'S'
UPPER_PAPER = 'P'
LOWER_ROCK = 'r'
LOWER_PAPER = 'p'
LOWER_SCISSOR = 's'
TYPE = 0
F_ROW = 1
F_COLUMN = 2
ROW = 0
COLUMN = 1
A_WIN = 1
B_WIN = 2
DRAW = 0
MAX_DISTANCE = 10
FIRST_ENTRY = 1
RE_ENTRY = 1
HIGHEST_RANK = 0

# Global variable:
# All the dictionary is made into global variable since all method is interact with them
upperPiecesDict: Dict[str, tuple] = {}
lowerPiecesDict: Dict[str, tuple] = {}
setBlocks = set()

targetsDict = {}
# keep track all the lower player's piece  <- to ensure no two Upper piece can target only one distinguish lower piece
targetedPiece = set()

# keep track of how many time a upper piece visit a tile while aiming for a target.
# this help rank the action.
positionHistory = {}

board: Dict[Tuple[int, int], str] = {}
'''
METHOD
'''


def get_weaker_piece(piece: str) -> str:
    """
    get the weaker piece type in low case
    :param piece:  key of the piece {e.g: 'r1'}
    :return: a single character the follow the rule: P> R> S> P
    """
    if piece[TYPE] in [LOWER_ROCK, UPPER_ROCK]:
        return LOWER_SCISSOR
    elif piece[TYPE] in [LOWER_PAPER, UPPER_PAPER]:
        return LOWER_ROCK
    return LOWER_PAPER


def get_stronger_piece(piece: str) -> str:
    """
    get the stronger piece type in low case
    :param piece: key of the piece {e.g: 'R1'}
    :return: a single character that follow the rule: P> R> S> P
    """
    if piece[TYPE] in [LOWER_ROCK, UPPER_ROCK]:
        return LOWER_PAPER
    elif piece[TYPE] in [LOWER_PAPER, UPPER_PAPER]:
        return LOWER_SCISSOR
    return LOWER_ROCK


def make_board():
    """
    add all the piece into a dictionary of board:
    (1) -> help with illustrate the current board
    (2) -> faster look up using a location.
    """
    global board

    for piece in lowerPiecesDict:
        board[lowerPiecesDict[piece]] = piece
    for piece in upperPiecesDict:
        board[upperPiecesDict[piece]] = piece
    for block in setBlocks:
        board[block] = BLOCK


def check_valid_action(piece: str, new_position: Tuple[int, int]) -> bool:
    """
    check if the action is resolved successfully.
    -> no piece should defeat any other piece unless the one whom it defeated is its Target.
    -> cant move on top the Block
    :param piece: we investigate
    :param new_position: result after perform the action
    :return: True if the action is successful. False if there are invalid move
    """
    # check if if the action happen {if it fail the new_position will be same as piece's position}
    if compare_tile(upperPiecesDict[piece], new_position):
        return False

    # check if new_position is the piece's target
    if piece in targetsDict.keys() and compare_tile(targetsDict[piece], new_position):
        return True

    # check if the position result from an action is into a free tile.
    if new_position in board.keys():
        if board[new_position] == BLOCK:
            return False
        if result_of_collision(board[new_position], piece) != DRAW:
            return False

    return True


def check_in(position: Tuple[int, int], piece: str):
    """
    log each time piece visit a tile.
    -> if the piece comeplete its journey {arrived at its Target's location}
    :param position: of the piece
    :param piece: that we want to log
    """
    global positionHistory
    # first time visit this tile.
    if position not in positionHistory[piece].keys():
        positionHistory[piece][position] = FIRST_ENTRY
    else:
        positionHistory[piece][position] += RE_ENTRY


def rank(position: Tuple[int, int], piece: str) -> int:
    """
    rank is base on how far of interested position to piece's target position
    -> how ever position is will be rank lower if it is visit before
    :param position: we want to rank
    :param piece: rank base on the piece
    :return: rank
    """
    # base rank is the distance between current position to the piece's target
    baseRank = distance_between(position, targetsDict[piece])
    # reduce_factor is base on how many time has this piece visit current tile.
    reduceFactor = 0
    if piece in positionHistory.keys() and position in positionHistory[piece].keys():
        reduceFactor = positionHistory[piece][position]

    return baseRank + reduceFactor


def result_of_collision(pieceA: str, pieceB: str) -> int:
    """
     Our upper pieces are R, P and S, lower pieces are r, p and s
     We will convert both to lower case letters and check each case
     Would be nice to use comparator but can't have r>s>p>r using it

    :param pieceA: type of the token in {'R','P','S','r','p','s'}
    :param pieceB: type of the token in {'R','P','S','r','p','s'}
    :return:  A_WIN, B_WIN or DRAW
    """
    pieceA = pieceA[TYPE].lower()
    pieceB = pieceB[TYPE].lower()
    if pieceA == LOWER_ROCK:
        if pieceB == LOWER_SCISSOR:
            return A_WIN
        elif pieceB == LOWER_PAPER:
            return B_WIN

    elif pieceA == LOWER_SCISSOR:
        if pieceB == LOWER_PAPER:
            return A_WIN
        elif pieceB == LOWER_ROCK:
            return B_WIN

    elif pieceA == LOWER_PAPER:
        if pieceB == LOWER_ROCK:
            return A_WIN
        elif pieceB == LOWER_SCISSOR:
            return B_WIN
    return DRAW


def slide_action(piece: str) -> list:
    """
    for each piece there will be 6 total position as result of slide action.
    -> check each to ensure they are valid move
    :param piece: will perform an slide action
    :return: unsorted list of all the valid action
    """
    action_list = []

    position = upperPiecesDict[piece]
    for action in (slide_right(position), slide_left(position), slide_up_left(position),
                   slide_up_right(position), slide_down_left(position), slide_down_right(position)):
        if check_valid_action(piece, action):
            action_list.append(action)

    return action_list


def swing_action(piece: str) -> List[Tuple[int, int]]:
    """
    for each adjacent piece there will be at most 3 swing move.
    -> check those move to ensure they are valid
    :param piece: will perform swing action
    :return: unsorted list of all valid action
    """

    # find all the adjacent piece
    adjacent_pieces = []

    position = upperPiecesDict[piece]
    for tile in (slide_right(position), slide_left(position), slide_up_left(position),
                 slide_up_right(position), slide_down_left(position), slide_down_right(position)):
        # check if the move is not out of board
        if not compare_tile(position, tile):
            # the tile can be any object except block
            if tile in board and board[tile] != BLOCK:
                adjacent_pieces.append(tile)

    # if there are at least one adjacent piece, add the position result from performing swing action from those piece.
    action_list = []

    for adjacent_piece in adjacent_pieces:
        for action in (swing_to_tile_1(position, adjacent_piece), swing_to_tile_2(position, adjacent_piece),
                       swing_to_tile_3(position, adjacent_piece)):
            if check_valid_action(piece, action):
                action_list.append(action)

    return action_list


def find_target(piece: str):
    """
    Find a weaker piece to this piece which closest
    -> only add if that potential target is not targeted by any other upper piece.
    :param piece: which we want to map a target to
    """
    global targetsDict
    # get the target
    targetType = get_weaker_piece(piece)

    targetDistance = MAX_DISTANCE
    target = ""

    for lowerPiece in lowerPiecesDict:
        if lowerPiece[TYPE] == targetType and lowerPiece not in targetedPiece:

            distance = distance_between(upperPiecesDict[piece], lowerPiecesDict[lowerPiece])

            if distance < targetDistance:
                target = lowerPiece
                targetDistance = distance
    if targetDistance != MAX_DISTANCE:
        targetsDict[piece] = lowerPiecesDict[target]
        targetedPiece.add(target)


def make_ranked_move_list(piece: str) -> List[Tuple[int, int]]:
    """
    add all the possible position after perform an action.
    sort them according to their rank{low to high}
    :param piece: will be perform the move
    :return: sorted list of position
    """

    # add all the valid adjacent position result from perform a slide action
    position_list = slide_action(piece)

    # add all the valid position result from perform a swing action
    position_list.extend(swing_action(piece))

    # rank the list base on how close it is to its target. The least visited position the higher rank it is.
    if piece in targetsDict.keys():
        position_list.sort(key=(lambda x: rank(x, piece)))
    return position_list


def perform_optimal_combination(move_list: dict):
    """
    perform an action from each of the piece sorted action list.
    -> prioritise action that after complete bring piece to its target.
    -> then choose an action from those piece with the least choice.
    -> then choose action from the remained piece in a way that not cause collision
    :param move_list: contain all the potential moved for all piece
    """
    global turn
    priority_list = []
    # For each piece who has a target, move all the piece that after take this action will lead directly to its target.
    # Keep the rest in the another list {so that we can sort them and , move each of them}
    for piece in move_list:
        if piece in targetsDict.keys() and compare_tile(move_list[piece][HIGHEST_RANK], targetsDict[piece]):

            print_move(upperPiecesDict[piece], targetsDict[piece])
            # found a piece !!!perform swap and update board
            del board[upperPiecesDict[piece]]

            upperPiecesDict[piece] = targetsDict[piece]
            board[targetsDict[piece]] = piece

        else:
            # this piece is not matching.
            priority_list.append(piece)

    # sort the list of remained piece base on the number of option they have {a.k.a move(s)}
    priority_list.sort(key=(lambda key: len(move_list[key])))

    # from those remained piece, move the least option first.
    for piece in priority_list:
        index = HIGHEST_RANK
        moved = False

        # each piece have to move once and only once
        while not moved:
            if index >= len(move_list[piece]):
                break

            # if current action lead to that piece collide with another piece, abandon it and go examine next action
            if move_list[piece][index] in board.keys() and\
                    result_of_collision(board[move_list[piece][index]], piece) != DRAW:
                index += 1
                continue

            # Found a move that does not cause collision
            print_move(upperPiecesDict[piece], move_list[piece][index])

            if upperPiecesDict[piece] in board.keys():
                del board[upperPiecesDict[piece]]
            upperPiecesDict[piece] = move_list[piece][index]
            board[move_list[piece][index]] = piece

            # log that this piece had visit this tile <- help with ranking its option.
            check_in(move_list[piece][index], piece)
            moved = True

def take_turn():
    """
    each turn: map out the board in order to look up easily.

    choose a combination of move that is partially optimal {move all the piece <some time> closer to its target}
    """

    possible_actions = {}
    for piece in upperPiecesDict:
        possible_actions[piece] = make_ranked_move_list(piece)
    global turn
    turn += 1
    perform_optimal_combination(possible_actions)
    update_target_dictionary()


def update_target_dictionary():
    """
    removed all piece that hit its target.
    attempt to assign it a new target.
    :return:
    """
    global  targetsDict, lowerPiecesDict, positionHistory
    removed = False
    deleted = []
    for piece in targetsDict.keys():
        if compare_tile(upperPiecesDict[piece], targetsDict[piece]):
            # remove the defeated piece
            removed_piece = ""
            for removed_p in lowerPiecesDict:
                if compare_tile(lowerPiecesDict[removed_p], targetsDict[piece]):
                    removed_piece = removed_p
                    break
            deleted.append(piece)
            del lowerPiecesDict[removed_piece]
            targetedPiece.remove(removed_piece)

            # reset the log for visited node
            positionHistory[piece] = {}
            removed = True

    for piece in deleted:
        del targetsDict[piece]
    # once the piece hit it target, it is free to find a new target.
    # if there are more lower piece then which already targeted {some lower piece is not target}
    # we can check if the available pieces can target those
    if removed and len(lowerPiecesDict) > len(targetsDict):
        for piece in upperPiecesDict:
            # when a piece is remove then there will
            if piece in targetsDict.keys():
                continue

            find_target(piece)

# Every tool in this document doesn't seem to be helpful. We're brute forcing this
def print_move(start: Tuple[int, int], finish: Tuple[int, int]):
    # Go through every slide comparsion. If not printed already it's a swing.
    # Three lines for readability
    # Horizontal
    global turn
    if abs(start[0] - finish[0]) == 0 and abs(start[0] - finish[0]) <= 1:
        print_slide(turn, start[0], start[1], finish[0], finish[1])
    # LL to UR diagonal or UR to LL diagonal
    elif abs(start[0] - finish[0]) <= 1 and abs(start[1] - finish[1]) == 0:
        print_slide(turn, start[0], start[1], finish[0], finish[1])
    # LR to UL diagonal
    elif start[0] == (finish[0] - 1) and start[0] == (finish[0] + 1):
        print_slide(turn, start[0], start[1], finish[0], finish[1])
    # UL to LR diagonal
    elif start[0] == (finish[0] + 1) and start[0] == (finish[0] - 1):
        print_slide(turn, start[0], start[1], finish[0], finish[1])
    else:
        print_swing(turn, start[0], start[1], finish[0], finish[1])