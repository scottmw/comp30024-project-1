from search.movement_logic import *
from search.util import print_board

# BLOCK should be "" but put [X] so we can observe from the print_board statement
BLOCK = "[X]"
POSITION_CLOSEST_TO_TARGET = 0
TYPE = 0
ROW = 1
COLUMN = 2
A_WIN = 1
B_WIN = 2
DRAW = 0
MAX_DISTANCE = 99
FIRST_CHAR = 0

upperDictPieces = {}
lowerDictPieces ={}
targetDict = {}
targeted_l_piece = set()
setBlocks = set()

# to handle the issue of hit dead end by just choose the minimal action.
# { piece_key: {
#       (row, column) : #visit
#       ...
#       }
# }
# each time a token visit a tile: mark it , the more time it visit a tile, the less appeal it is .
visit_record = {}

def make_board(lowerPieces, upperPieces, setBlocks):
    """
    done
    create a board of the current game -> can do a position look up.
    :param upperPieces: dictionary contain all the upper piece and its location
    :param lowerPieces: dictionary contain all the lower piece and its location
    :param setBlocks: all the block
    :return: the dictionary represent the board
    """
    board = {}
    for piece in lowerPieces:
        board[lowerPieces[piece]] = piece

    for piece in upperPieces:
        board[upperPieces[piece]] = piece

    for block in setBlocks:
        board[block] = BLOCK
    return board


def get_stronger_piece(piece_type):
    """
    done
    Stronger piece is base on the type, even if the piece are from a player.
    :param piece_type: the type of the piece that we are interested
    :return: the type of the piece that stronger than the input piece
    """
    if piece_type[0] == 'R':
        return 'p'
    if piece_type[0] == 'S':
        return 'r'
    return 's'


def add_slide_action(upperDict, piece, board):
    """
    done
    add all the valid slide action to a list
    :param board: dictionary contain all piece and block
    :param upperDict: contain detail about upper piece
    :param piece: key of the interested piece
    :return: un sorted list of all position as a result of slide action
    """
    list = []

    # check the right tile
    appending_list = add_if_valid(list, piece, slide_right(upperDict[piece]), upperDict[piece], board)

    # check the left tile
    appending_list = add_if_valid(list, piece, slide_left(upperDict[piece]), upperDict[piece], board)

    # check the up_left
    appending_list = add_if_valid(list, piece, slide_up_left(upperDict[piece]), upperDict[piece], board)

    # check the up_right
    appending_list = add_if_valid(list, piece, slide_up_right(upperDict[piece]), upperDict[piece], board)

    # check the down_left
    appending_list = add_if_valid(list, piece, slide_down_left(upperDict[piece]), upperDict[piece], board)

    # check the down_right
    appending_list = add_if_valid(list, piece, slide_down_right(upperDict[piece]), upperDict[piece], board)

    return appending_list


def add_if_valid(position_list, piece, new_position, piece_position, board):
    """
    done
    check if the move is valid.
    :param position_list:  list contain all added slide action from this turn
    :param piece: the interested upper piece
    :param new_position: position result of a slide action
    :param piece_position:  initial position of the piece.
    :param board: dictionary contain all piece and block
    :return: the list
    """

    # check if the new position is result of a out of board action
    if compare_tile(new_position, piece_position):
        return position_list

    if new_position in board.keys():
        # check if the tile is occupied by a block
        if board[new_position] == BLOCK:
            return position_list

        # check if the new position is occupied by piece that stronger
        elif board[new_position][0] == get_stronger_piece(piece):
            return position_list

    # add the new position and return
    position_list.append(new_position)
    return position_list


def make_priority_list_of_action(upperDict, piece, targetDict, board):
    """
    done
    compile all possible action this piece can under go.
    sort them base on the result distance relative to the piece's target
    :param upperDict: use to read the position of the piece
    :param piece: key of the piece
    :param targetDict: dictionary contain the target of every upper piece
    :param board: the dictionary of board
    :return: the sorted list of position result from an action
    """
    # add all the adjacent move to queue
    position_list = add_slide_action(upperDict, piece, board)

    # add all the swing action to queue
    position_list.extend(add_swing_action(upperDict, piece, board))

    # sort the list base on the how close it is to target
    if piece in targetDict.keys():
        position_list.sort(key=(lambda x: rank_by_appeal(x, targetDict,piece)))
    if position_list[0] in visit_record[piece].keys():
        if visit_record[piece][position_list[0]] > 4 and piece in targetDict.keys():
            # check all the move in the visit list, if all above 4, then this piece cant reach the target.
            for position in position_list[1::]:
                if visit_record[piece][position] < 4:
                    return position_list
            del targetDict[piece]
    return position_list


def update_state(upperPieces, lowerPieces, setBlocks, targetDict):
    """
    done
    move the piece in a way that bring all piece closer to its target
    # currently only in away that is work for one piece.
    :param upperPieces: dictionary contain all the upper piece
    :param lowerPieces: dictionary contain all the lower piece
    :param setBlocks: dictionary contain all the block on the board
    :param targetDict: map each piece to a target
    :return: the updated upper piece
    """
    # create the board in order to support faster look up
    board = make_board(lowerPieces, upperPieces, setBlocks)
    print_board(board)
    possible_action = {}
    # right now, i assume there only 1 piece, there will be more code combine all the queue.
    for piece in upperPieces:
        possible_action[piece] = make_priority_list_of_action(upperPieces, piece, targetDict, board)

    upperPieces = choose_optimal_combination(possible_action, upperPieces, board, targetDict)
    return upperPieces


def choose_optimal_combination(possible_action, upperPieces, board, targetDict):

    """
    done
    prioritise action lead a piece directly to its target.
    else choose a combination that does not cause collision between upper piece

    :param possible_action: all set of possible move sorted base on how close they are to the piece target
    :param upperPieces: contain all upper piece
    :param board: all piece
    :param targetDict: map all piece to its target
    :return:
    """
    finished = set()
    priority_list = []
    for piece in possible_action.keys():
        if piece in possible_action.keys():

            if piece in targetDict.keys() and compare_tile(possible_action[piece][0], targetDict[piece]):

                # perform swap and update board
                del board[upperPieces[piece]]
                upperPieces[piece] = targetDict[piece]

                board[targetDict[piece]] = piece
                # we've finished with this piece.
                finished.add(piece)
            else:
                priority_list.append(piece)

    priority_list.sort(key=(lambda x: len(possible_action[x])))
    # each piece is moved once and only once.
    for piece in priority_list:
        if piece in finished:
            continue
        index = 0
        moved = False
        while not moved:
            if index >= len(possible_action[piece]):
                break;
            # check if the move exist in the board
            if possible_action[piece][index] not in board.keys():
                # perform swap and update board
                del board[upperPieces[piece]]
                upperPieces[piece] = possible_action[piece][index]

                board[possible_action[piece][index]] = piece
                moved = True

                decrease_appeal(possible_action[piece][index], piece)
            else:
                # check if no collision happen:
                if piece_collision(board[possible_action[piece][index]], piece) == DRAW:
                    # perform swap and update board
                    del board[upperPieces[piece]]
                    upperPieces[piece] = possible_action[piece][index]

                    board[possible_action[piece][index]] = piece
                    moved = True

                    decrease_appeal(possible_action[piece][index], piece)
            index += 1

    return upperPieces


def decrease_appeal(position, piece):
    """
    done
    :param position:
    :param piece:
    :return:
    """
    global visit_record

    if position not in visit_record[piece].keys():
        visit_record[piece][position] = 1
    else:
        visit_record[piece][position] += 1
    return visit_record


def check_if_piece_hit_target(upperPieces, lowerPieces, targetDict):
    """
    done
    remove the target from the target dictionary if the upper piece is at its target location
    :param upperPiece: contain all upper piece
    :param targetDict: map upper piece to its lower target
    :return: the updated target dictionary
    """
    removed = False
    for piece in upperPieces:
        if piece in targetDict.keys() and compare_tile(upperPieces[piece], targetDict[piece]):
            for removed_piece in lowerPieces:
                if compare_tile(lowerPieces[removed_piece],targetDict[piece]):
                   removed = removed_piece
            del targetDict[piece]
            del lowerPieces[removed]
            # reset the visited node since piece start a new journey
            visit_record[piece] = {}
            removed = True
    # once the piece hit it target, it is free to find a new target.
    # if there are more lower piece then which already targeted {some lower piece is not target}
    # we can check if the available pieces can target those
    if removed and len(lowerPieces) > len(targetDict):
        for piece in upperPieces:
            # when a piece is remove then there will
            if piece in targetDict:
                continue

            find_target(piece)

    return targetDict


def piece_collision(pieceA, pieceB) -> int:
    """
    done
     Our upper pieces are R, P and S, lower pieces are r, p and s
     We will convert both to lower case letters and check each case
     Would be nice to use comparator but can't have r>s>p>r using it

     Return A_WIN if pieceA wins, B_WIN if pieceB wins and DRAW if neither win
     Only look at first character of string

    :param pieceA: type of the token in {'R','P','S','r','p','s'}
    :param pieceB: type of the token in {'R','P','S','r','p','s'}
    :return: A_WIN, B_WIN or DRAW
    """
    pieceA = pieceA[TYPE].lower()[0]
    pieceB = pieceB[TYPE].lower()[0]
    if pieceA == "r":
        if pieceB == "s":
            return A_WIN
        elif pieceB == "p":
            return B_WIN

    elif pieceA == "s":
        if pieceB == "p":
            return A_WIN
        elif pieceB == "r":
            return B_WIN

    elif pieceA == "p":
        if pieceB == "r":
            return A_WIN
        elif pieceB == "s":
            return B_WIN

    return DRAW


def add_swing_action(upperDict, piece, board):
    """
    done
    check for adjacent tile.
    if there are any adjacent tile to this add swing action from those tile
    :param upperDict: contain all the upper piece
    :param piece: which we want to move
    :param board: dictionary contain all piece in the game
    :return: list of all action added
    """
    adjacent_list = []
    # check the right tile
    right_tile = slide_right(upperDict[piece])
    if not compare_tile(right_tile, upperDict[piece]) and\
            right_tile in board and board[right_tile] != BLOCK:
        adjacent_list.append(right_tile)

    # check the left tile
    left_tile = slide_left(upperDict[piece])
    if not compare_tile(left_tile, upperDict[piece]) and\
            left_tile in board and board[left_tile] != BLOCK:
        adjacent_list.append(left_tile)

    # check the up_left
    up_left_tile = slide_up_left(upperDict[piece])
    if not compare_tile(up_left_tile, upperDict[piece]) and\
            up_left_tile in board and board[up_left_tile] != BLOCK:
        adjacent_list.append(up_left_tile)

    # check the up_right
    up_right_tile = slide_up_right(upperDict[piece])
    if not compare_tile(up_right_tile, upperDict[piece]) and\
            up_right_tile in board and board[up_right_tile] != BLOCK:
        adjacent_list.append(up_right_tile)

    # check the down_left
    down_left_tile = slide_down_left(upperDict[piece])
    if not compare_tile(down_left_tile, upperDict[piece]) and\
            down_left_tile in board and board[down_left_tile] != BLOCK:
        adjacent_list.append(down_left_tile)

    # check the down_right
    down_right_tile = slide_down_right(upperDict[piece])
    if not compare_tile(down_right_tile, upperDict[piece]) and\
            down_right_tile in board and board[down_right_tile] != BLOCK:
        adjacent_list.append(down_right_tile)

    # if there are at least one adjacent piece : add the adjacent move from  those tile
    position_list = []
    for adjacent_piece in adjacent_list:
        # add the swing action to tile 1 (look at swing_action section in movement_logic modulo)
        position_list = add_if_valid(position_list, piece, swing_to_tile_1(upperDict[piece], adjacent_piece),
                                     upperDict[piece], board)

        # add the swing action to tile 2
        position_list = add_if_valid(position_list, piece, swing_to_tile_2(upperDict[piece], adjacent_piece),
                                    upperDict[piece], board)

        # add the swing action to tile 3
        position_list = add_if_valid(position_list, piece, swing_to_tile_3(upperDict[piece], adjacent_piece),
                                    upperDict[piece], board)

    return position_list


def find_target(piece_key):
    """
    done
    This function changes the value of the key given in the
    target dictionary to the closest enemy piece it can beat
    XUAN: by separated the upper to lower piece, we dont need to search for the whole dictionary every time we compare.

    :param piece_key: key of the piece to find a target for. We should only work with upper player's pieces
    :return: null
    """
    global targetDict
    # Set the target
    if piece_key[TYPE] == "R":
        target = "s"
    elif piece_key[TYPE] == "S":
        target = "p"
    else:
        target = "r"

    # Now we check which target is closest
    # Start with dummy information
    targetPiece = ""
    targetDist = MAX_DISTANCE
    for key in lowerDictPieces:
        if key[FIRST_CHAR] == target and key not in targeted_l_piece:
            distance = distance_between(upperDictPieces[piece_key], lowerDictPieces[key])
            # if the distance between this key and the query key is less than the least distance.
            # as well as the key is not targeted by any other key.
            if distance < targetDist:
                targetPiece = key
                targetDist = distance

    # Now add target to dictionary
    # case where no key that match criteria.
    if targetDist == MAX_DISTANCE:
        return
    else:
        targetDict[piece_key] = lowerDictPieces[targetPiece]
        targeted_l_piece.add(targetPiece)


def rank_by_appeal(x, targetDict, piece):
    """
    done
    :param x:
    :param targetDict:
    :param piece:
    :return:
    """
    # distance_between(x, targetDict[piece])
    # make_priority_list_of_action(upperDict, piece, targetDict, board)
    # the reduce would base on how close it is to its target and how many time we visit this.
    base = distance_between(x, targetDict[piece])
    reduce_factor = 0

    if x in visit_record[piece].keys():
        reduce_factor = visit_record[piece][x]

    adjust_score = base +    reduce_factor

    return adjust_score
