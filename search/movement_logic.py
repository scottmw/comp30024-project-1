"""
:pram coordinate a tuple contain 2 number (r, q)
:return  coordinate of the token after the action is complete
        if the new position is out of the board:
            return the token'
            s position instead.
row = r
column = q

Slide action logic:
        token only able to slide to the tile that are numbered {1 -> 6}

        #              .-'-._.-'-.
        #             |  3  |  4  |
        #           .-'-._.-'-._.-'-.
        #          |  1  |token|  2  |
        #          '-._.-'-._.-'-._.-'
        #             |  5  |  6  |
        #             '-._.-'-._.-'
        #
"""
from typing import Tuple

ROW = 0
COLUMN = 1


# 1
def slide_left(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW], coordinate[COLUMN] - 1)

        if not check_within_board(new_pos):
            return coordinate
    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


# 2
def slide_right(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW], coordinate[COLUMN] + 1)

        if not check_within_board(new_pos):
            return coordinate

    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


# 3
def slide_up_left(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW] + 1, coordinate[COLUMN] - 1)

        if not check_within_board(new_pos):
            return coordinate

    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


# 4
def slide_up_right(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW] + 1, coordinate[COLUMN])

        if not check_within_board(new_pos):
            return coordinate

    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


# 5
def slide_down_left(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW] - 1, coordinate[COLUMN])

        if not check_within_board(new_pos):
            return coordinate

    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


# 6
def slide_down_right(coordinate: Tuple[int, int]) -> Tuple[int, int]:
    if coordinate:
        new_pos = (coordinate[ROW] - 1, coordinate[COLUMN] + 1)

        if not check_within_board(new_pos):
            return coordinate

    else:
        print("Empty coordinate")
        return -10, -10

    return new_pos


####################################################################################################
LEFT = 1
RIGHT = 2
UP_LEFT = 3
UP_RIGHT = 4
DOWN_LEFT = 5
DOWN_RIGHT = 6
OUT_OF_BOARD = 0
"""
:argument piece - the selected game object that is presented by its position on board { (row, column)}
          x     - the adjacent game object
:return   new position after the action is complete       
            if the new position is out of the board return the token's position instead   

Swing action logic:
        x is the another token that current token swing from
            -> token can only swing to adjacent tiles{1,2,3} to [x] that are not adjacent to itself
            tile 1, 2 and 3 are those token can swing to.
     
     
        RIGHT              .-'-.   UP_RIGHT      .-'-._.-'-.
                          |  1  |               |  1  |  2  |
                  .-'-._.-'-._.-'-.             '-._.-'-._.-'-.
                 |piece| [x] |  2  |               | [x] |  3  |
                 '-._.-'-._.-'-._.-'             .-'-._.-'-._.-'
                          |  3  |               |piece|      
                          '-._.-'               '-._.-'      
        
        OWN_RIGHT    .-'-.        UP_LEFT        .-'-._.-'-.
                    |piece|                     |  2  |  3  |
                    '-._.-'-._.-'-.           .-'-._.-'-._.-'  
                       | [x] |  1  |         |  1  | [x] |      
                     .-'-._.-'-._.-'         '-._.-'-._.-'-.    
                    |  3  |  2  |                     |piece|
                    '-._.-'-._.-'                     '-._.-'
        
        DOWN_LEFT           .-'-.  LEFT            .-'-.      
                           |piece|                |  3  |  
                   .-'-._.-'-._.-'              .-'-._.-'-._.-'-.  
                  |  3  | [x] |                |  2  | [x] |piece|    
                  '-._.-'-._.-'-.              '-._.-'-._.-'-._.-'   
                     |  2  |  1  |                |  1  |      
                     '-._.-'-._.-'                '-._.-'      
"""


def swing_to_tile_1(piece: Tuple[int, int], x: Tuple[int, int]) -> Tuple[int, int]:
    position = get_relative_position(piece, x)
    new_position = ()
    if position == LEFT:
        new_position = slide_down_left(slide_left(piece))

    if position == RIGHT:
        new_position = slide_up_right(slide_right(piece))

    if position == UP_RIGHT:
        new_position = slide_up_left(slide_up_right(piece))

    if position == UP_LEFT:
        new_position = slide_left(slide_up_left(piece))

    if position == DOWN_LEFT:
        new_position = slide_down_right(slide_down_left(piece))

    if position == DOWN_RIGHT:
        new_position = slide_right(slide_down_right(piece))

    # if the position of the token after the action complete is out of board, return the token position in
    # stead
    if compare_tile(new_position, x):
        return piece

    return new_position


def swing_to_tile_2(piece: Tuple[int, int], x: Tuple[int, int]) -> Tuple[int, int]:
    position = get_relative_position(piece, x)

    new_position = ()
    if position == LEFT:
        new_position = slide_left(slide_left(piece))

    if position == RIGHT:
        new_position = slide_right(slide_right(piece))

    if position == UP_RIGHT:
        new_position = slide_up_right(slide_up_right(piece))

    if position == UP_LEFT:
        new_position = slide_up_left(slide_up_left(piece))

    if position == DOWN_LEFT:
        new_position = slide_down_left(slide_down_left(piece))

    if position == DOWN_RIGHT:
        new_position = slide_down_right(slide_down_right(piece))

    # if the position of the token after the action complete is out of board, return the token position in
    # stead
    if compare_tile(new_position, x):
        return piece

    return new_position


def swing_to_tile_3(piece: Tuple[int, int], x: Tuple[int, int]) -> Tuple[int, int]:
    position = get_relative_position(piece, x)

    new_position = ()
    if position == LEFT:
        new_position = slide_up_left(slide_left(piece))

    if position == RIGHT:
        new_position = slide_down_right(slide_right(piece))

    if position == UP_RIGHT:
        new_position = slide_right(slide_up_right(piece))

    if position == UP_LEFT:
        new_position = slide_up_right(slide_up_left(piece))

    if position == DOWN_LEFT:
        new_position = slide_left(slide_down_left(piece))

    if position == DOWN_RIGHT:
        new_position = slide_down_left(slide_down_right(piece))

    # if the position of the token after the action complete is out of board, return the token position in
    # stead
    if compare_tile(new_position, x):
        return piece

    return new_position


def get_relative_position(piece: Tuple[int, int], x: Tuple[int, int]) -> int:
    """
    :param x: the adjacent game object
    :argument piece: the selected game object that is presented by its position on board { (row, column)}
    :return   the position of x relative to token
                  -> using the slide direction as the position definition.
                     e.g left,right, up_left, up_right,..etc
    """
    # check if x is to the left of tile
    if compare_tile(slide_left(piece), x):
        return LEFT

    # check if x is to the right of tile
    if compare_tile(slide_right(piece), x):
        return RIGHT

    # check if x is to the up_left of the tile
    if compare_tile(slide_up_left(piece), x):
        return UP_LEFT

    # check if x is to the up_right of the tile
    if compare_tile(slide_up_right(piece), x):
        return UP_RIGHT

    # check if x is to the down_left of the tile
    if compare_tile(slide_down_left(piece), x):
        return DOWN_LEFT

    # check if x is to the down_right of the tile
    if compare_tile(slide_down_right(piece), x):
        return DOWN_RIGHT


def compare_tile(tile1: Tuple[int, int], tile2: Tuple[int, int]) -> bool:
    """
    :param tile2: compose of (row, column)
    :param tile1: compose of (row, column)
    :return true if both of the row and column are matches between 2 tiles
    """
    return tile1[ROW] == tile2[ROW] and tile1[COLUMN] == tile2[COLUMN]


def check_within_board(tile: Tuple[int, int]) -> bool:
    """
    ensure that the tile is exist within the board
    :param tile: coordinate of the tile
    :return: true if the tile exist within the board, else false
    """
    if tile[ROW] < -4 or tile[ROW] > 4:
        return False
    if tile[COLUMN] < -4 or tile[COLUMN] > 4:
        return False

    if tile[COLUMN] in [-4, -3, -2, -1] and tile[ROW] < -4 - tile[COLUMN]:
        return False

    if tile[COLUMN] in [4, 3, 2, 1] and tile[ROW] > 4 - tile[COLUMN]:
        return False
    return True


def distance_between(piece: Tuple[int, int], target: Tuple[int, int]) -> bool:
    """
    :argument piece - compose of (row, column)
    :argument target - compose of (row, column)
    :return the number of step from upper's token to lower's token

    Taken from https://www.redblobgames.com/grids/hexagons/
    under section: double coordinates for doublewidth
    ->
    """

    dx = abs(piece[1] - target[1])
    dy = abs(piece[0] - target[0])
    result = dy + max(0, (dx - dy) / 2)
    return result
